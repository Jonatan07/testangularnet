import { Component } from '@angular/core';

@Component({
  selector: 'persona-root',
  templateUrl: './persona.component.html',
  styleUrls: ['./persona.component.css']
})

export class PersonaComponent {
  title = 'Angular';
  tituloPersona = "";
  onCreatePersona() {
    alert("Uff esto es angular !!!");
  }
  onModificarPersona(event: Event)
  {
    this.tituloPersona = (<HTMLInputElement>event.target).value;
  }
}
